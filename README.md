# Вычислительная геометрия

## Запуск:

Установка необходимых библиотек:

```
python3 -m venv venv
source venv/bin/activate  
pip install -r requirements.txt
```

Из дериктории  `app` запустить выполнение python скрипта `manage.py` с тремя параметрами командной строки
 
 - название задачи: `minimum_area_rectangle`, `monotone_poly`
 - файл с входным выпуклым полиномом
 - директория куда записывать результат
 - флаг; `True`, если надо сохранить визуализацию работы алгоритма 

## Минимальный описанный прямоугольник для выпуклого многоугольника (калиперы)

Пример вызова:

```
cd app
python3 manage.py minimum_area_rectangle ./res/input.txt ./res True 
```

Выход программы:

```
start programme with
 -- input file = "./res/input.txt"
 -- output dir = "./res"
 -- visual mode = True
algorithm visualization saved to "./res/algorithm_visual.png"
algorithm result visualization saved to "./res/output_visual.png"
algorithm result "./res/output.txt"
```

## Монотонный прямоульник относительно заданного направления

Пример входного файла:

```
# направление
1 3 
# вершины многоугольника
2 2  
1 2
2 4
5 5
5 3
```

Пример вызова:

```
cd app
python3 manage.py monotone_poly ./res/input.txt ./res True 
```

Выход программы:

```
start programme with
 -- input file = "./res/monotone_poly/input_5.txt"
 -- output dir = "./res/monotone_poly"
 -- visual mode = True
polygon size: 5
total time for solving: 0 ms
algorithm result saved to "./res/monotone_poly/output.txt"
```

## Дополнительные настройки

Рядом с файлом `manage.py` лежит файл `config.json`. Он позволяет настроить дополнительные опции работы программы:

 - `visualization::vertex: bool` - параметр отвечающий за отображение номеров вершин многоуголников
 - `visualization::direction: bool` - параметр отвечающий за отображение направлений отрезков
 - `visualization::algorithm: bool` - отрисовывать ход решения задачи или нет
 - `visualization::output: bool` - отрисовывать результат
 - `TOLERANCE`: точность с которой будет решаться задача (используется при сравнении принадлежность трех точек одной прямой)