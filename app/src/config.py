import json, os


class Config:

    vertex = True
    direction = True
    vis_algorithm = True
    vis_output = True
    print_steps = True
    TOLERANCE = 1e-10

    @staticmethod
    def configure():
        path = os.path.abspath(os.path.dirname(__name__) + 'config.json')
        with open(path) as f:
            data = json.load(f)
            Config.vertex = data["visualization"]["vertex"]
            Config.direction = data["visualization"]["direction"]
            Config.vis_algorithm = data["visualization"]["algorithm"]
            Config.vis_output = data["visualization"]["output"]
            Config.print_steps = data["visualization"]["print_steps"]
            Config.TOLERANCE = data["TOLERANCE"]