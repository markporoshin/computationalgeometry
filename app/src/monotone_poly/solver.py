import numpy as np


def turn_around_polygon(polygon):
    return np.flip(polygon, 0)


def is_clockwise_polygon(polygon) -> bool:
    for i in range(len(polygon)):
        edge1 = np.concatenate((polygon[i+1] - polygon[i], [0]))
        edge2 = np.concatenate((polygon[i+2] - polygon[i+1], [0]))
        if np.cross(edge1, edge2)[2] == 0:
            continue
        return np.cross(edge1, edge2)[2] < 0


def read_data(file_path: str) -> tuple[list, list, bool]:
    dir: list = []
    vertexes = []
    with open(file_path) as file:
        i = 0
        for line in file.readlines():
            if line.startswith('#'):
                continue
            if i == 0:
                dir = list(map(int, line.split(' ')))
                i += 1
                continue
            vertexes.append(list(map(int, line.split(' '))))
            i += 1

    vertexes = np.array(vertexes, dtype=int)

    is_clockwise = is_clockwise_polygon(polygon=vertexes)

    if not is_clockwise:
        vertexes = turn_around_polygon(polygon=vertexes)
        print("counterclockwise polygon, fixed")
    print(f"polygon size: {len(vertexes)}")
    return dir, list(vertexes.tolist()), is_clockwise


def sign(a):
    return 1 if a > 0 else -1 if a < 0 else 0


def triangle_area(edge1, edge2):
    return edge1[0] * edge2[1] - edge1[1] * edge2[0]


def solve(dir, poly):
    ortho_dir = -dir[1], dir[0]
    poly_size = len(poly)

    target_vertex_id = -1 % poly_size
    support_vertex_id = (-1 + 1) % poly_size
    v1 = poly[target_vertex_id]
    v2 = poly[support_vertex_id]
    v3 = poly[target_vertex_id][0] + ortho_dir[0], poly[target_vertex_id][1] + ortho_dir[1]
    edge1 = v2[0] - v1[0], v2[1] - v1[1]
    edge2 = v3[0] - v1[0], v3[1] - v1[1]
    last_sgn = sign(triangle_area(edge1, edge2))

    changing_sng = 0

    for i in range(poly_size):
        target_vertex_id = i % poly_size
        support_vertex_id = (i + 1) % poly_size
        v1 = poly[target_vertex_id]
        v2 = poly[support_vertex_id]
        v3 = poly[target_vertex_id][0] + ortho_dir[0], poly[target_vertex_id][1] + ortho_dir[1]

        edge1 = v2[0] - v1[0], v2[1] - v1[1]
        edge2 = v3[0] - v1[0], v3[1] - v1[1]

        current_sng = sign(triangle_area(edge1, edge2))
        if current_sng == 0:
            continue
        if current_sng + last_sgn == 0:
            changing_sng += 1
        last_sgn = current_sng

    return changing_sng == 2