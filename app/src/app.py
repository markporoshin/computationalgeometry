from src.config import Config
from src.polygon import load_polygon
from src.minimum_area_rectangle_fast.solver import solve
from src.monotone_poly.solver import solve as monotonous_poly_solve, read_data
from matplotlib import pyplot as plt

import time


def minimum_area_rectangle_fast(input, output_dir, visual):
    input_poly, is_clockwise = load_polygon(input)
    start = time.time() * 1000
    result = solve(input_poly, is_clockwise, visual=visual)
    end = time.time() * 1000
    print(f"total time for solving: {int(end - start)} ms")

    if visual:
        if Config.vis_algorithm:
            path = output_dir + '/algorithm_visual.png'
            plt.savefig(path)
            print(f'algorithm visualization saved to "{path}"')
            plt.clf()

        if Config.vis_output:
            result.plot()
            path = output_dir + '/output_visual.png'
            plt.legend()
            plt.savefig(path)
            print(f'algorithm result visualization saved to "{path}"')

    path = output_dir + '/output.txt'

    result.save(path)
    print(f'algorithm result "{path}"')


def monotonous_poly(input, output_dir, visual):
    dir, input_poly, is_clockwise = read_data(input)
    start = time.time() * 1000
    answer = monotonous_poly_solve(dir, input_poly)
    end = time.time() * 1000
    print(f"total time for solving: {int(end - start)} ms")

    path = output_dir + '/output.txt'
    with open(path, 'w') as output_file:
        output_file.write(str(answer))
    print(f'algorithm result saved to "{path}"')


if __name__ == '__main__':
    visual = True
    monotonous_poly('/', '/', True)




