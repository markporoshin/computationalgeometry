from matplotlib import pyplot as plt
import numpy as np

from src.config import Config


def plot_dir_line(start, end, color='r', width=0.001, figure=None, ax=None, direction=Config.direction):
    if ax is None:
        ax = plt.gca()

    if figure is None:
        figure = plt.gcf()

    row = (end[0] + start[0]) / 2, (end[1] + start[1]) / 2
    d = (end[0] - start[0]) / 10, (end[1] - start[1]) / 10
    plt.plot([start[0], end[0]], [start[1], end[1]], color, linewidth=0.1)
    if direction and Config.direction:
        plt.arrow(row[0] - d[0] / 2, row[1] - d[1] / 2, d[0], d[1], width=width, head_width=np.abs(d).mean(),
                  shape='full', lw=0, length_includes_head=True)
    plt.grid()


def plot_poly(poly, figure=None, ax=None, color='r', vertex=Config.vertex, direction=Config.direction):
    if ax is None:
        ax = plt.gca()

    if figure is None:
        figure = plt.gcf()

    poly = list(poly)

    for i, (start, end) in enumerate(zip(poly, poly[1:] + [poly[0]])):
        plot_dir_line(start, end, ax=ax, figure=figure, color=color, direction=direction)
        if vertex and Config.vertex:
            ax.annotate(f'v.{str(i)}', (start[0], start[1]))
    plt.grid()


def plot_vertexes(vertexes, c='r', marker='*', s=5, label='-'):
    x = list(map(lambda v: v[0], vertexes))
    y = list(map(lambda v: v[1], vertexes))
    plt.scatter(x, y, c=c, marker=marker, s=s, label=label)
    plt.grid()


def plot_mar(mar, lim=True, figure=None, ax=None, color='r'):
    if ax is None:
        ax = plt.gca()

    if figure is None:
        figure = plt.gcf()

    if lim:
        min_x = mar.poly[:, 0].min()
        max_x = mar.poly[:, 0].max()
        min_y = mar.poly[:, 1].min()
        max_y = mar.poly[:, 1].max()

        shape = max(max_x - min_x, max_y - min_y)
        x_margin = shape - (max_x - min_x) / 2 + 1
        y_margin = shape - (max_y - min_y) / 2 + 1

        plt.xlim([min_x - x_margin, max_x + x_margin])
        plt.ylim([min_y - y_margin, max_y + y_margin])
    plot_poly(mar.poly, ax=ax, figure=figure, color=color)

    for i, caliper in enumerate(mar.calipers):
        plot_dir_line(caliper.point, caliper.point + np.array(caliper.dir), color='g')
