import numpy as np
from fractions import Fraction

from src.config import Config
from matplotlib import pyplot as plt
from src.minimum_area_rectangle_fast.models.mar import Result, MAR, Line, Caliper
from src.visual import plot_dir_line, plot_poly


def plot_mar(mar: MAR, lim=True, figure=None, ax=None, color='r'):
    if ax is None:
        ax = plt.gca()

    if figure is None:
        figure = plt.gcf()

    min_x = mar.poly[:, 0].min()
    max_x = mar.poly[:, 0].max()
    min_y = mar.poly[:, 1].min()
    max_y = mar.poly[:, 1].max()

    shape = max(max_x - min_x, max_y - min_y)
    x_margin = shape - (max_x - min_x) / 2 + 1
    y_margin = shape - (max_y - min_y) / 2 + 1
    if lim:
        plt.xlim([min_x - x_margin, max_x + x_margin])
        plt.ylim([min_y - y_margin, max_y + y_margin])
    plot_poly(mar.poly, ax=ax, figure=figure, color=color)

    for i, caliper in enumerate(mar.calipers):
        plot_dir_line(caliper.point, caliper.point + np.array(caliper.dir) * shape, color='g', width=0.01)


def rotate_vertexes(vertexes: np.array, angle):
    rot = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    return np.dot(vertexes, rot)


def rotate_vertex(vertex: np.array, angle):
    rot = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    return np.dot(vertex, rot)


def recalculate_calipers(mar: MAR, ind):
    poly = mar.poly
    poly_size = len(poly)
    start_ind = mar.calipers[ind].link_vertex_id
    end_ind = (mar.calipers[ind].link_vertex_id + 1) % poly_size
    mar.calipers[ind].support_vertex_id = mar.calipers[ind].link_vertex_id
    mar.calipers[ind].link_vertex_id = (mar.calipers[ind].link_vertex_id + 1) % poly_size
    mar.calipers[ind].point = mar.poly[mar.calipers[ind].link_vertex_id]
    dir = poly[end_ind][0] - poly[start_ind][0], poly[end_ind][1] - poly[start_ind][1]
    mar.calipers[(ind + 0) % 4].dir = dir
    mar.calipers[(ind + 1) % 4].dir = dir[1], -dir[0]
    mar.calipers[(ind + 1) % 4].support_vertex_id = None
    mar.calipers[(ind + 2) % 4].dir = -dir[0], -dir[1]
    mar.calipers[(ind + 2) % 4].support_vertex_id = None
    mar.calipers[(ind + 3) % 4].dir = -dir[1], dir[0]
    mar.calipers[(ind + 3) % 4].support_vertex_id = None


def count_angle(v1, v2):
    unit_vector_1 = v1 / np.linalg.norm(v1)
    unit_vector_2 = v2 / np.linalg.norm(v2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    return angle


def triangle_square(edge1, edge2):
    return edge1[0] * edge2[1] - edge1[1] * edge2[0]


def caliper_index2rotate(mar: MAR) -> int:
    poly_size = len(mar.poly)
    poly = mar.poly
    c1 = mar.calipers[0]
    c2 = mar.calipers[1]
    c3 = mar.calipers[2]
    c4 = mar.calipers[3]
    # calipers 1 & 3
    start_ind_c1 = c1.link_vertex_id % poly_size
    end_ind_c1 = (c1.link_vertex_id + 1) % poly_size
    inverse_edge_c1 = poly[start_ind_c1][0] - poly[end_ind_c1][0], poly[start_ind_c1][1] - poly[end_ind_c1][1]
    start_ind_c3 = c3.link_vertex_id % poly_size
    end_ind_c3 = (c3.link_vertex_id + 1) % poly_size
    edge_c3 = poly[end_ind_c3][0] - poly[start_ind_c3][0], poly[end_ind_c3][1] - poly[start_ind_c3][1]

    hor_ind = 0
    edge_c1_c3 = inverse_edge_c1
    if triangle_square(inverse_edge_c1, edge_c3) > 0:
        hor_ind = 2
        edge_c1_c3 = edge_c3
    # calipers 2 & 4
    start_ind_c2 = c2.link_vertex_id % poly_size
    end_ind_c2 = (c2.link_vertex_id + 1) % poly_size
    inverse_edge_c2 = poly[start_ind_c2][0] - poly[end_ind_c2][0], poly[start_ind_c2][1] - poly[end_ind_c2][1]
    start_ind_c4 = c4.link_vertex_id % poly_size
    end_ind_c4 = (c4.link_vertex_id + 1) % poly_size
    edge_c4 = poly[end_ind_c4][0] - poly[start_ind_c4][0], poly[end_ind_c4][1] - poly[start_ind_c4][1]

    ver_ind = 1
    edge_c2_c4 = inverse_edge_c2
    if triangle_square(inverse_edge_c2, edge_c4) > 0:
        ver_ind = 3
        edge_c2_c4 = edge_c4
    edge_c2_c4 = [-edge_c2_c4[1], edge_c2_c4[0]]

    if triangle_square(edge_c1_c3, edge_c2_c4) > 0:
        return ver_ind
    return hor_ind


def step(mar: MAR) -> bool:
    caliper_index = caliper_index2rotate(mar)
    recalculate_calipers(mar, ind=caliper_index)
    return mar.calipers[2].link_vertex_id != mar.start_0_vertex


def get_bound_ids(polygon: np.array):
    def argmin(values):
        return min(range(len(values)), key=values.__getitem__)

    bottom_ind = argmin(list(map(lambda v: v[1], polygon)))
    top_ind = argmin(list(map(lambda v: -v[1], polygon)))
    left_ind = argmin(list(map(lambda v: v[0], polygon)))
    right_ind = argmin(list(map(lambda v: -v[0], polygon)))
    return top_ind, right_ind, bottom_ind, left_ind


def get_start_mar(polygon: np.array):
    mar = MAR(poly=polygon)

    dirs = ((1, 0), (0, -1), (-1, 0), (0, 1))
    bound_ids = get_bound_ids(polygon=polygon)
    for vertex_id, dir in zip(bound_ids, dirs):
        caliper = Caliper(
            link_vertex_id=vertex_id,
            point=polygon[vertex_id],
            dir=dir
        )
        mar.calipers.append(caliper)
    mar.start_0_vertex = mar.calipers[0].link_vertex_id
    return mar


def build_rect(founder_vertexes, support_vertexes) -> tuple[np.array, tuple]:
    founder_dir = founder_vertexes[1][0] - founder_vertexes[0][0], founder_vertexes[1][1] - founder_vertexes[0][1]
    other_dir = -founder_dir[1], founder_dir[0]

    l1 = Line(point=founder_vertexes[0], dir=founder_dir)
    l2 = Line(point=support_vertexes[0], dir=other_dir)
    l3 = Line(point=support_vertexes[1], dir=founder_dir)
    l4 = Line(point=support_vertexes[2], dir=other_dir)

    v1_x, v1_y = Line.intersection(l1, l2)
    v2_x, v2_y = Line.intersection(l2, l3)
    v3_x, v3_y = Line.intersection(l3, l4)
    v4_x, v4_y = Line.intersection(l4, l1)

    a_x = v2_x - v1_x
    a_y = v2_y - v1_y
    b_x = v2_x - v3_x
    b_y = v2_y - v3_y

    a_n = a_x * a_x + a_y * a_y
    b_n = b_x * b_x + b_y * b_y

    s = a_n * b_n

    return [
        [v1_x, v1_y],
        [v2_x, v2_y],
        [v3_x, v3_y],
        [v4_x, v4_y],
    ], s


def build_result(mar: MAR, is_clockwise: bool) -> Result:
    founder_vertexes_ind = []
    founder_vertexes = []
    founder_caliper_id = 0
    for i, caliper in enumerate(mar.calipers):
        if caliper.support_vertex_id is not None:
            founder_caliper_id = i
            founder_vertexes_ind.append(caliper.link_vertex_id)
            founder_vertexes_ind.append(caliper.support_vertex_id)
            founder_vertexes.append(caliper.point)
            founder_vertexes.append(mar.poly[caliper.support_vertex_id])
            break

    sc_1 = (founder_caliper_id + 1) % 4
    sc_2 = (founder_caliper_id + 2) % 4
    sc_3 = (founder_caliper_id + 3) % 4
    support_vertexes_ind = [
        mar.calipers[sc_1].link_vertex_id,
        mar.calipers[sc_2].link_vertex_id,
        mar.calipers[sc_3].link_vertex_id,
    ]
    support_vertexes = [
        mar.calipers[sc_1].point,
        mar.calipers[sc_2].point,
        mar.calipers[sc_3].point,
    ]

    rect, s = build_rect(founder_vertexes, support_vertexes)
    poly_size = len(mar.poly)
    poly = mar.poly
    if not is_clockwise:
        founder_vertexes_ind = [poly_size - vertex_ind - 1 for vertex_ind in founder_vertexes_ind]
        support_vertexes_ind = [poly_size - vertex_ind - 1 for vertex_ind in support_vertexes_ind]
    return Result(
        min_s=s,
        poly=poly,
        rect=rect,
        founder_vertexes=founder_vertexes_ind,
        support_vertexes=support_vertexes_ind,
        is_clockwise=is_clockwise
    )


def solve(polygon: np.array, is_clockwise: bool, visual=False) -> Result:
    mar = get_start_mar(polygon=polygon)
    step(mar)
    opt_result = build_result(mar, is_clockwise)
    if visual and Config.print_steps:
        print(f"step 0: square={opt_result.min_s}, ".ljust(40, ' ') + f"optimal_square={opt_result.min_s}".ljust(40, ' ')
              + f"founder_vertexes={opt_result.founder_vertexes} support_vertexes={opt_result.support_vertexes}")
    i = 1
    while True:
        is_continue = step(mar)
        result = build_result(mar, is_clockwise)
        if visual and Config.print_steps:
            print(
                f"step {i}: square={result.min_s}, ".ljust(40, ' ') + f"optimal_square={result.min_s}".ljust(40, ' ')
                + f"founder_vertexes={result.founder_vertexes} support_vertexes={result.support_vertexes}")
        if opt_result.min_s > result.min_s:
            opt_result = result
        i += 1
        if not is_continue:
            break
    return opt_result
