from dataclasses import dataclass, field
from typing import Optional
from fractions import Fraction

import numpy as np

from src.visual import plot_poly, plot_vertexes
from src.polygon import turn_around_polygon

@dataclass
class Line:
    point: np.array
    dir: np.array

    @staticmethod
    def intersection(l1, l2):
        A = - l1.dir[1]
        B = l1.dir[0]
        C = A * l1.point[0] + B * l1.point[1]

        D = - l2.dir[1]
        E = l2.dir[0]
        F = D * l2.point[0] + E * l2.point[1]

        det = A * E - B * D
        det_x = C * E - B * F
        det_y = A * F - C * D

        return Fraction(det_x, det), Fraction(det_y, det)


@dataclass
class Caliper(Line):
    link_vertex_id: int
    support_vertex_id: Optional[int] = None


@dataclass
class MAR:
    poly: np.array
    calipers: list[Caliper] = field(default_factory=list)
    start_0_vertex: int = 0


@dataclass
class Result:
    min_s: tuple

    poly: np.array

    rect: np.array

    founder_vertexes: list
    support_vertexes: list

    is_clockwise: True

    def save(self, filename):
        rect_str = ',\n'.join([', '.join([str(v) for v in row]) for row in self.rect])

        with open(filename, 'w') as out:
            out.write(f"founder_vertexes: {self.founder_vertexes}\n")
            out.write(f"support_vertexes: {self.support_vertexes}\n")
            out.write(f"the area of the rect: {self.min_s}\n")
            out.write(f"rect:\n{str(rect_str)}")

    def plot(self):
        pass
        plot_poly(self.rect, vertex=False, direction=False)
        poly = self.poly
        if not self.is_clockwise:
            poly = turn_around_polygon(poly)

        plot_poly(poly, vertex=True, direction=False)
        v = list(poly[v] for v in self.founder_vertexes)
        plot_vertexes(v, c='r', s=70, label='founder_vertexes')
        v = list(poly[v] for v in self.support_vertexes)
        plot_vertexes(v, c='g', s=20, label='support_vertexes')
        plot_vertexes(self.rect, c='m', s=20, label='rect')

    def less(self, result):

        pass