import numpy as np


def load_polygon(file_path: str) -> tuple[np.array, bool]:
    vertexes = []
    with open(file_path) as file:
        for i, line in enumerate(file.readlines()):
            if line.startswith('#'):
                continue
            vertexes.append(list(map(int, line.split(' '))))

    vertexes = np.array(vertexes, dtype=int)

    is_clockwise = is_clockwise_polygon(polygon=vertexes)

    if not is_clockwise:
        vertexes = turn_around_polygon(polygon=vertexes)
        print("counterclockwise polygon, fixed")
    print(f"polygon size: {len(vertexes)}")
    return vertexes.tolist(), is_clockwise


def turn_around_polygon(polygon):
    return np.flip(polygon, 0)


def is_clockwise_polygon(polygon):
    for i in range(len(polygon)):
        edge1 = np.concatenate((polygon[i+1] - polygon[i], [0]))
        edge2 = np.concatenate((polygon[i+2] - polygon[i+1], [0]))
        if np.cross(edge1, edge2)[2] == 0:
            continue
        return np.cross(edge1, edge2)[2] < 0


def save_polygon(file_path: str, poly: np.array):
    with open(file_path, 'w') as file:
        for vertex in poly:
            file.write(f'{vertex[0]} {vertex[1]}\n')