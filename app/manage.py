import sys

from src.app import minimum_area_rectangle_fast
from src.app import monotonous_poly
from src.config import Config

if __name__ == '__main__':
    Config.configure()

    problem = sys.argv[1]
    input_file = sys.argv[2]
    output_dir = sys.argv[3]
    visual = sys.argv[4] == 'True'

    print(f'start programme with\n -- input file = "{input_file}"\n -- output dir = "{output_dir}"\n -- visual mode = {visual}')
    if problem == 'monotone_poly':
        monotonous_poly(input_file, output_dir, visual)
    elif problem == 'minimum_area_rectangle':
        minimum_area_rectangle_fast(input_file, output_dir, visual)
    else:
        print('incorrect problem name')

